import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomeAr extends StatefulWidget {
  @override
  _HomeArState createState() => _HomeArState();
}

class _HomeArState extends State<HomeAr> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    saveUrl();
  }

  void saveUrl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("selectedUrl", 'ar');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: WebView(
          initialUrl: 'https://falatat.com/',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
        ),
      ),
    );
  }
}
