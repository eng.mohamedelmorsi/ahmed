import 'package:ahmedapp/splash.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Home.dart';
import 'Homear.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String lang;
  @override
  void initState() {
    super.initState();
    getFromCash();
  }

  void getFromCash() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String selectedLang = prefs.getString("selectedUrl");
    setState(() {
      lang = selectedLang;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Falatat',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:  () {
        if (lang == "ar") {
          return HomeAr();
        } else if (lang == "en") {
          return Home();
        } else {
          return Splash();
        }
      }(),
    );
  }
}
